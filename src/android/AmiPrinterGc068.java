package com.example.plugin;
import hdx.HdxUtil;
import android_serialport_api.SerialPort;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;

public class AmiPrinterGc068{
  // from MyApplication.java
  protected SerialPort mSerialPort = null;
  protected OutputStream mOutputStream;
  protected InputStream mInputStream;

  public SerialPort getSerialPort() throws SecurityException, IOException, InvalidParameterException, Exception {
      if (mSerialPort == null) {
          String path = HdxUtil.GetPrinterPort();//dev/ttyS3
          int baudrate = 115200;//Integer.decode(sp.getString("BAUDRATE", "-1"));
          mSerialPort = new SerialPort(path, baudrate, 0);
      }
      return mSerialPort;
  }
  // public SerialPort getSerialPortUSB() throws SecurityException, IOException, InvalidParameterException {
  //     if (mSerialPort == null) {
  //         String path = "/dev/bus/usb/002/015";//dev/ttyS3
  //         int baudrate = 115200;//Integer.decode(sp.getString("BAUDRATE", "-1"));
  //         mSerialPort = new SerialPort(path, baudrate, 0);
  //     }
  //     return mSerialPort;
  // }
  // private void sendCommand(int... command) {
  //   try {
  //     for (int i = 0; i < command.length; i++) {
  //       mOutputStream.write(command[i]);
  //     }
  //   } catch (IOException e) {
  //     e.printStackTrace();
  //   }
  // }

  public String getval(String toprint){
    try{
      HdxUtil.SwitchSerialFunction(HdxUtil.SERIAL_FUNCTION_PRINTER);
      HdxUtil.SetPrinterPower(1);// power on the printer
    }
    catch(Exception e) {
      return "HdxInit Fail [546] c";
    }
    try {
      // if(toprint.charAt(0)=='_'){
      //   mSerialPort = getSerialPortUSB();
      // }else{
        mSerialPort = getSerialPort();
      // }
      mOutputStream = mSerialPort.getOutputStream();
      mInputStream = mSerialPort.getInputStream();
      mOutputStream.write(toprint.getBytes());
      // mOutputStream.write("Just a test, AmiH is a Genius!".getBytes());
      // sendCommand(0x0a, 0x0a, 0x0a, 0x0a);
      mOutputStream.flush();
      /* Create a receiving thread */
      // mReadThread = new ReadThread();
      // mReadThread.start();
    } catch (SecurityException e) {
      return "AHA[SecurityException c]";
    } catch (IOException e) {
      return "AHA[IOException c]";
    } catch (InvalidParameterException e) {
      return "AHA[InvalidParameterException c]";
    } catch(Exception e) {
      return "General error [921] c";
    }
    return "[done c]";
  }
}

//   // sendCommand(0x0a);
//   // sendCommand(0x1B, 0x23, 0x23, 0x53, 0x4C, 0x41, 0x4E, 0x0f); // china
//   // sendCommand(0x1D, 0x21, 0x01); // double height
//   // sendCommand(0x1b, 0x61, 1);
//   // mOutputStream.write("倍高命令   double height".getBytes("cp936"));
//   // sendCommand(0x0a);
//   // sendCommand(0x1D, 0x21, 0x00); // cancel double height
//   // mOutputStream.write("取消倍高命令取消倍高命 cancel double height".getBytes("cp936"));
//   // sendCommand(0x0a);
//   // sendCommand(0x1D, 0x21, 0x10); // double width
//   // mOutputStream.write("倍宽命令  double width ".getBytes("cp936"));
//   // sendCommand(0x0a);
//   // sendCommand(0x1D, 0x21, 0x00); // cancel double width
//   // mOutputStream.write("取消倍宽命令取消倍宽命令取消倍宽命令 cancel double width".getBytes("cp936"));
//   // sendCommand(0x0a);
//   // mOutputStream.write("english test".getBytes());
//   // sendCommand(0x0a);
// }
