package com.example.plugin;
import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;

import com.example.plugin.AmiPrinterGc068;

public class Hello extends CordovaPlugin {
  @Override
  public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {
    AmiPrinterGc068 amiprinter = new AmiPrinterGc068();
    if (action.equals("greet")) {
      String name = data.getString(0);
      String message = "Hello-final?, " + amiprinter.getval(name);// + Integer.toString(amiprinter.init())
      callbackContext.success(message);
      return true;
    } else {
      callbackContext.success("no function called 087yt9bg");
      return false;
    }
  }
}
