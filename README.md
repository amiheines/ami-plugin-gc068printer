# AmiPluginPrinterGc068POS
based off: https://github.com/don/cordova-plugin-hello
Simple plugin that returns your string prefixed with hello.
## Using
Create a new Cordova Project

    $ phonegap create hello com.example.helloapp Hello
Install the plugin

    $ cd hello
    $ phonegap plugin add https://bitbucket.org/amiheines/ami-plugin-gc068printer.git
Edit `www/js/index.js` and add the following code inside `onDeviceReady`
```js
    var success = function(message) {
        alert(message);
    }
    var failure = function() {
        alert("Error calling Hello Plugin");
    }
    hello.greet("World", success, failure);
```
Install Android platform

    phonegap platform add android
Run the code

    phonegap run android
